-- copyright 2019 quinn
-- available for use under gpl 3 or later, see LICENSE for details
{-# LANGUAGE TemplateHaskell #-}
import System.Exit (ExitCode(..))
import Control.Exception

import System.Process

import System.Directory
import System.FilePath.Posix

import Data.Time

import Data.Char
import Data.ByteUnits
import Text.Megaparsec as M
import Text.Megaparsec.Char as M
import Data.Void
import Control.Monad
import Control.Monad.IO.Class (liftIO)

import Brick as B
import Brick.Widgets.Center as B
import Brick.Widgets.Border as B
import Brick.Widgets.Border.Style as B
import Brick.BChan as B
import qualified Graphics.Vty as V
import Lens.Micro ((^.))
import Lens.Micro.TH (makeLenses)
import Control.Concurrent
import Data.Maybe


-- constants
batteryThreshold :: Int
batteryThreshold = 65

mailTag :: String
mailTag = "inbox"

mailExport :: IO (Maybe Int)
mailExport = do
  cm <- checkMail
  if cm == 0
    then return $ Nothing
    else return $ Just cm

checkMail :: IO Int
checkMail = do
  ms <- readProcessWithExitCode "notmuch" ["search", ("tag:" ++ mailTag)] []
  case handleProg ms of
    Just x -> return $ length $ lines x
    Nothing -> return 0

handleProg :: (ExitCode, a, c) -> Maybe a
handleProg (ExitSuccess, b, c) = Just b
handleProg _ = Nothing

isBackSlash :: Char -> Bool
isBackSlash '\\' = True
isBackSlash _ = False

loadExport :: IO String
loadExport = do
  la <- readFile "/proc/loadavg"
  return $ reverse $ removeUntil ' ' $ reverse la

removeUntil :: Eq a => a -> [a] -> [a]
removeUntil x (y:ys) =
  if x == y
  then ys
  else removeUntil x ys

artColon =
  "\n█\n\n█\n"

art0 =
  "███\n█ █\n█ █\n█ █\n███"

art1 =
  " █\n██\n █\n █\n███"

art2 =
  "███\n  █\n███\n█\n███"

art3 =
  "███\n  █\n ██\n  █\n███"

art4 =
  "█ █\n█ █\n███\n  █\n  █ "

art5 =
  "███\n█\n███\n  █\n███"

art6 =
  "███\n█\n███\n█ █\n███"

art7 =
  "███\n  █\n  █\n  █\n  █ "

art8 =
  "███\n█ █\n███\n█ █\n███"

art9 =
  "███\n█ █\n███\n  █\n███"

batDirP :: FilePath -> String -> Bool
batDirP [] ('B':xs) = batDirP ['B'] xs
batDirP ['B'] ('A':xs) = batDirP ['B','A'] xs
batDirP ['B','A'] ('T':xs) = True
batDirP _ _ = False

listBats :: IO [String]
listBats = do
  ld <- listDirectory "/sys/class/power_supply"
  return $ map (\x -> "/sys/class/power_supply" </> x) $ Prelude.filter (batDirP []) ld

batteryStat :: FilePath -> IO Int
batteryStat p = do
  f <- readFile $ p </> "capacity"
  return $ read $ filter isDigit f

batteryExport :: IO (Maybe Int)
batteryExport = do
  lb <- listBats
  if (length lb) == 0
    then return Nothing
    else do
    bs <- batteryStat $ lb !! 0
    if batteryThreshold > bs
      then return $ Just $ bs
      else return Nothing

localTimeFormat :: String -> IO String
localTimeFormat str = do
  zt <- getZonedTime
  return $ formatTime defaultTimeLocale str zt

timeExport :: IO String
timeExport = localTimeFormat "%H.%M"

dateExport :: IO String
dateExport = localTimeFormat "%D"

type Parser = Parsec Void String

data MemStat = MemStat String ByteValue deriving (Show)

memParser :: Parser MemStat
memParser = do
  key <- some $ oneOf $ ['a'..'z'] ++ ['A'..'Z'] ++ ['1'..'9'] ++ ['(',')'] ++ ['_']
  _ <- char ':'
  _ <- space
  val <- some digitChar
  _ <- many $ string " kB"
  _ <- eol
  return (MemStat key (ByteValue (read val :: Float) KiloBytes))

memInfoParser :: Parser [MemStat]
memInfoParser = manyTill memParser eof

memoryParse :: String -> [MemStat]
memoryParse file =
  case parse memInfoParser "/proc/meminfo" file of
    Left s -> error (show s)
    Right memstat -> memstat

memStatEq :: String -> MemStat -> Bool
memStatEq str ((MemStat x _))
  | x == str = True
  | otherwise = False

findMemStat :: String -> [MemStat] -> MemStat
findMemStat _ [] = error "not an available memory statistic"
findMemStat str (x:xs)
  | memStatEq str x = x
  | otherwise = findMemStat str xs

getMemStats :: IO [MemStat]
getMemStats = liftM memoryParse $ readFile "/proc/meminfo"

byteValueFromMemStat :: MemStat -> ByteValue
byteValueFromMemStat (MemStat _ x) = x

memDisplay :: [MemStat] -> String
memDisplay mem =
  (memDispShort $ freeMem mem)
  ++
  " / "
  ++
  (memDispShort $ findMemStat "MemTotal" mem)

memDispShort :: MemStat -> String
memDispShort mem =
  getShortHand $ getAppropriateUnits $ byteValueFromMemStat  mem

freeMem :: [MemStat] -> MemStat
freeMem mem =
  (MemStat "TrueFree"
  (ByteValue (
  memStatBytes "MemTotal" mem
  - memStatBytes "MemFree" mem
  - memStatBytes "Buffers" mem
  - memStatBytes "Cached" mem
  ) Bytes))

memStatBytes :: String -> [MemStat] -> Float
memStatBytes str mem =
  getBytes $ byteValueFromMemStat $ findMemStat str mem

printMem :: IO ()
printMem = do
  mem <- getMemStats
  putStrLn $ memDisplay mem

memoryExport :: IO String
memoryExport = do
  mem <- getMemStats
  return $ memDisplay mem

data Tick = Tick

data AppState = AppState
  { _mem :: String
  , _time :: String
  , _date :: String
  , _bat :: Maybe Int
  , _load :: String
  , _mail :: Maybe Int
  } deriving (Show)

makeLenses ''AppState

initState :: AppState
initState = AppState
  { _mem = "loading"
  , _time = "loading"
  , _date = "loading"
  , _bat = Nothing
  , _load = "loading"
  , _mail = Nothing
  }

ioState :: IO AppState
ioState = do
  memstr <- memoryExport
  timestr <- timeExport
  datestr <- dateExport
  batint <- batteryExport
  loadstr <- loadExport
  mailint <- mailExport
  return AppState { _mem = memstr
                  , _time = timestr
                  , _date = datestr
                  , _bat = batint
                  , _load = loadstr
                  , _mail = mailint
                  }

theApp :: App AppState Tick ()
theApp =
    App { appDraw = drawUI
        , appChooseCursor = neverShowCursor
        , appHandleEvent = appEvent
        , appStartEvent = return
        , appAttrMap = const $ attrMap V.defAttr []
}

drawUI :: AppState -> [Widget ()]
drawUI s =
  [ vBox [ batteryW s
         , mailW s
         , (hBox
            [ (B.hLimitPercent 50 (B.center $ vBox [ (B.hCenter (strWrap ((s ^. load))))
                                                 , (B.hCenter (strWrap ((s ^. mem))))
                                                 ]))
  , (vBorder)
  , (B.hLimitPercent 100 (B.center $ vBox [ (B.hCenter (strWrap ((s ^. time))))
                                        ,(B.hCenter (strWrap ((s ^. date))))
                                        ]))
  -- , (vBorder)
  -- , (hLimitPercent 100 (timeW (s ^. time)))
  ])]]

mailW :: AppState -> Widget ()
mailW s =
  case (s ^. mail) of
    Just n -> vBox [ B.hCenter $ strWrap $ (show n) ++ " new messages"
                   , hBorder]
    Nothing -> emptyWidget

batteryW :: AppState -> Widget ()
batteryW s =
  case (s ^. bat) of
    Just n -> vBox [ B.hCenter $ strWrap $ (show n) ++ "% battery"
                   , hBorder]
    Nothing -> emptyWidget

timeW :: String -> Widget ()
timeW (a:b:c:d:e:_) =
  hBox [ padLeftRight 1 $ strWrap $ timeWMap a
       , padLeftRight 1 $ strWrap $ timeWMap b
       , padLeftRight 1 $ strWrap $ timeWMap c
       , padLeftRight 1 $ strWrap $ timeWMap d
       , padLeftRight 1 $ strWrap $ timeWMap e
       ]

timeWMap :: Char -> String
timeWMap '.' = artColon
timeWMap '0' = art0
timeWMap '1' = art1
timeWMap '2' = art2
timeWMap '3' = art3
timeWMap '4' = art4
timeWMap '5' = art5
timeWMap '6' = art6
timeWMap '7' = art7
timeWMap '8' = art8
timeWMap '9' = art9
timeWMap _ = ""

appEvent state (VtyEvent (V.EvKey (V.KChar 'q') [])) = halt state
appEvent state _ = liftIO (ioState) >>= continue

main :: IO ()
main = do
    chan <- newBChan 10

    forkIO $ forever $ do
        writeBChan chan Tick
        threadDelay 2000000

    void $ customMain (V.mkVty V.defaultConfig) (Just chan) theApp initState
