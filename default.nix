let pkgs = import <nixpkgs> {};
in {
  fountain = pkgs.haskellPackages.callPackage ./fountain.nix {};
}
