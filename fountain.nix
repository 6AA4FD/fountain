{ mkDerivation, base, brick, byteunits, directory, filepath
, megaparsec, microlens, microlens-th, process, scientific, stdenv
, text, time, vty
}:
mkDerivation {
  pname = "fountain";
  version = "0.1.1.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base brick byteunits directory filepath megaparsec microlens
    microlens-th process scientific text time vty
  ];
  license = stdenv.lib.licenses.gpl3;
}
